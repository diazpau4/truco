using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrucoManager : MonoBehaviour
{
    public const int VALOR_MANO = 1;

    public const int VALOR_TRUCO_QUERIDO = 2;
    public const int VALOR_RETRUCO_QUERIDO = 3;
    public const int VALOR_VALECUATRO_QUERIDO = 4;
    public const int VALOR_TRUCO_NO_QUERIDO = 1;
    public const int VALOR_RETRUCO_NO_QUERIDO = 2;
    public const int VALOR_VALECUATRO_NO_QUERIDO = 3;

    public const int VALOR_ENVIDO_NO_QUERIDO = 1;
    public const int VALOR_ENVIDO_QUERIDO = 2;

    public const int VALOR_REAL_ENVIDO_NO_QUERIDO = 1;
    public const int VALOR_REAL_ENVIDO_QUERIDO = 3;

    public const int VALOR_ENVIDO_ENVIDO_NO_QUERIDO = 2;
    public const int VALOR_ENVIDO_ENVIDO_QUERIDO = 4;

    public const int VALOR_ENVIDO_REALENVIDO_NO_QUERIDO = 2;
    public const int VALOR_ENVIDO_REALENVIDO_QUERIDO = 5;

    public const int VALOR_ENVIDO_ENVIDO_REALENVIDO_NO_QUERIDO = 5;
    public const int VALOR_ENVIDO_ENVIDO_REALENVIDO_QUERIDO = 7;

    public const int VALOR_FALTA_ENVIDO_NO_QUERIDO = 1;
    public const int VALOR_ENVIDO_FALTA_ENVIDO_NO_QUERIDO = 2;
    public const int VALOR_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO = 3;
    public const int VALOR_ENVIDO_ENVIDO_FALTA_ENVIDO_NO_QUERIDO = 4;
    public const int VALOR_ENVIDO_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO = 5;
    public const int VALOR_ENVIDO_ENVIDO_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO = 7;

    public const int VALOR_FALTA_ENVIDO_QUERIDO = 30;

    public const int VALOR_FLOR = 3;

    public Mazo mazoCartas;
    public Jugador Jugador1, 
                   Jugador2;

    public GameObject BT1, BT2, BT3, Imagen, Texto, TextoPuntaje1, TextoPuntaje2;
    public Carta auxCarta1, auxCarta2;
    public int Rondas;

    public GameObject Ganaste1, 
                      Ganaste2;
    public GameObject BotonEnvido,
                      BotonEnvidoEnvido,
                      BotonRealEnvido,
                      BotonFaltaEnvido,
                      BotonTruco,
                      BotonReTruco,
                      BotonValeCuatro,
                      BotonFlor,
                      BotonQuiero,
                      BotonNoQuiero,
                      BotonIrAlMazo;

    public EstadoTruco EstadoActualTruco = EstadoTruco.SinCantar;
    public enum EstadoTruco
    {
        SinCantar,
        TrucoCantado,
        RetrucoCantado,
        ValeCuatroCantado,
    }

    public EstadoEnvido EstadoActualEnvido = EstadoEnvido.SinCantar;
    public enum EstadoEnvido
    {
        SinCantar,
        EnvidoCantado,
        EnvidoEnvidoCantado,
        RealEnvidoCantado,
        EnvidoRealEnvidoCantado,
        EnvidoEnvidoRealEnvidoCantado,
        FaltaEnvidoCantado,
        EnvidoFaltaEnvidoCantado,
        RealEnvidoFaltaEnvidoCantado,
        EnvidoEnvidoFaltaEnvidoCantado,
        EnvidoRealEnvidoFaltaEnvidoCantado,
        EnvidoEnvidoRealEnvidoFaltaEnvidoCantado,
        YaJugado,
    }

    public Text TurnoText;
    public Text CantaText;

    public bool TurnoJugador1Completado,
                TurnoJugador2Completado;

    public bool JuegaJugador1;
    public bool CantaJugador1;

    public void Update()
    { 
        if (Input.GetMouseButtonDown(0))
        {
            if (Imagen.activeSelf)
            {
                Imagen.SetActive(false);
            }
            if (Texto.activeSelf)
            {
                Texto.SetActive(false);
            }
        }

        ActivarReactivarCartas(CantaJugador1);
        ActualizarTextoPuntajes();
    }
    void Start()
    {
        mazoCartas = FindObjectOfType<Mazo>();
        DarTurno();
        Jugador1.Inicializar(mazoCartas.DameTresCartas());
        Jugador2.Inicializar(mazoCartas.DameTresCartas());
        auxCarta1 = new Carta(0, "z", 0, 0);
        auxCarta2 = new Carta(0, "x", 0, 0);
        IniciarEstadoBotones();
        Imagen.SetActive(false);
        Texto.SetActive(false);
    }

    public void ActivarReactivarCartas(bool MostrarJugador1)
    {
        Jugador1.ActivarCartas(MostrarJugador1);
        Jugador2.ActivarCartas(!MostrarJugador1);
        ActualizarEstadoBotonCartas(MostrarJugador1);
    }

    public void ActualizarEstadoBotonCartas(bool MostrarCartasJugador1)
    {
        Jugador JugadorActual;

        if (MostrarCartasJugador1)
        {
            JugadorActual = Jugador1;
        } else
        {
            JugadorActual = Jugador2;
        }

        if (JugadorActual.MisTresCartas[0].FueUsada)
        {
            EstadoBoton (BT1, false);
        } else
        {
            EstadoBoton (BT1, true);
        }
        if (JugadorActual.MisTresCartas[1].FueUsada)
        {
            EstadoBoton (BT2, false);
        } else
        {
            EstadoBoton (BT2, true);
        }
        if (JugadorActual.MisTresCartas[2].FueUsada)
        {
            EstadoBoton (BT3, false);
        } else
        {
            EstadoBoton (BT3, true);
        }
    }

    public void CuandoSeJuegaCarta(int Button)
    {
        if (JuegaJugador1)
        {
            JuegaJugador1 = false;
            CantaJugador1 = false;
            TurnoText.text = "2";
            CantaText.text = "2";

            TurnoJugador1Completado = true;
            Imagen.SetActive(true);
            Texto.SetActive(true);
            auxCarta1 = Jugador1.MisTresCartas[Button-1];
            auxCarta1.FueUsada = true;
            Texto.GetComponentInChildren<Text>().text = "Jugador 1 jug�:" + auxCarta1.Palo + " " + auxCarta1.Numero;
        } 
        else
        {
            JuegaJugador1 = true;
            CantaJugador1 = true;
            TurnoText.text = "1";
            CantaText.text = "1";

            TurnoJugador2Completado = true;
            Imagen.SetActive(true);
            Texto.SetActive(true);
            auxCarta2 = Jugador2.MisTresCartas[Button - 1];
            auxCarta2.FueUsada = true;
            Texto.GetComponentInChildren<Text>().text = ("Jugador 2 jugo: " + auxCarta2.Palo + " " + auxCarta2.Numero);
        }

        if (TurnoJugador1Completado && TurnoJugador2Completado)
        {
            Rondas++;
            Imagen.SetActive(true);
            Texto.SetActive(true);
            if (auxCarta1.Valor < auxCarta2.Valor)
            {
                JuegaJugador1 = true;
                CantaJugador1 = true;
                Jugador1.RondasGanadas++;
                MostrarMensajeGanoRondaJ1();
            }
            else if (auxCarta2.Valor < auxCarta1.Valor)
            {
                JuegaJugador1 = false;
                CantaJugador1 = false;
                Jugador2.RondasGanadas++;
                MostrarMensajeGanoRondaJ2();
            } else
            {
                // caso de parda (cartas con un mismo valor)
                Jugador1.RondasGanadas++;
                Jugador2.RondasGanadas++;
                MostrarMensajeParda();
            }

            auxCarta1 = null;
            auxCarta2 = null;
            ChequearRondas();
        }

        if (Rondas >= 1) 
        {
            EstadoBoton (BotonFlor, false);
            EstadoBoton (BotonEnvido, false);
            EstadoBoton (BotonEnvidoEnvido, false);
            EstadoBoton (BotonRealEnvido, false);
            EstadoBoton (BotonFaltaEnvido, false);
        } else
        {
            ActualizarEstadoBotonFlor ();
        }
    }

    public void MostrarMensajeGanoRondaJ2()
    {
        Texto.GetComponentInChildren<Text>().text = ("El jugador 2 ha ganado la ronda");
    }

    public void MostrarMensajeGanoRondaJ1()
    {
        Texto.GetComponentInChildren<Text>().text = ("El jugador 1 ha ganado la ronda");
    }

    public void MostrarMensajeParda()
    {
        Texto.GetComponentInChildren<Text> ().text = ("Parda!");
    }

    public void EstadoBoton(GameObject go, bool Activado)
    {
        Button Boton = go.GetComponent<Button>();
        Boton.enabled = Activado;
        Image Imagen = go.GetComponent<Image>();
        Imagen.enabled = Activado;
    }

    public void IniciarEstadoBotones()
    {
        EstadoBoton(BotonQuiero, false);
        EstadoBoton(BotonNoQuiero, false);

        EstadoBoton(BotonTruco, true);
        EstadoBoton(BotonReTruco, false);
        EstadoBoton(BotonValeCuatro, false);

        EstadoBoton(BotonEnvido, true);
        EstadoBoton(BotonFaltaEnvido, true);
        EstadoBoton(BotonRealEnvido, true);
        EstadoBoton(BotonEnvidoEnvido, false);

        ActualizarEstadoBotonFlor ();
    }

    public void ActualizarEstadoBotonFlor()
    {
        if (EstadoActualEnvido == EstadoEnvido.SinCantar && EstadoActualTruco == EstadoTruco.SinCantar)
        {
            Jugador JugadorActual;

            if (JuegaJugador1)
            {
                JugadorActual = Jugador1;
            } else
            {
                JugadorActual = Jugador2;
            }

            if (JugadorActual.TieneFlor () && Rondas == 0)
            {
                EstadoBoton (BotonFlor, true);
            } else
            {
                EstadoBoton (BotonFlor, false);
            }
        } else
        {
            EstadoBoton (BotonFlor, false);
        }
    }

    public void ActualizarQuienCanta ()
    {
        if (CantaJugador1)
        {
            CantaJugador1 = false;
            CantaText.text = "2";
        } else
        {
            CantaJugador1 = true;
            CantaText.text = "1";
        }
    }

    public void TocoBotonTruco()
    {
        ActualizarQuienCanta();

        EstadoActualTruco = EstadoTruco.TrucoCantado;

        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
        EstadoBoton(BotonReTruco, true);
        EstadoBoton(BotonTruco, false);

        EstadoBoton(BotonEnvido, false);
        EstadoBoton(BotonEnvidoEnvido, false);
        EstadoBoton(BotonRealEnvido, false);
        EstadoBoton(BotonFaltaEnvido, false);

        ActualizarEstadoBotonFlor();
    }

    public void TocoBotonReTruco()
    {
        ActualizarQuienCanta();

        EstadoActualTruco = EstadoTruco.RetrucoCantado;

        EstadoBoton(BotonReTruco, false);
        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
        EstadoBoton(BotonValeCuatro, true);
    }

    public void TocoBotonVale4()
    {
        ActualizarQuienCanta();

        EstadoActualTruco = EstadoTruco.ValeCuatroCantado;

        EstadoBoton(BotonValeCuatro, false);
        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
    }

    public void TocoBotonEnvido()
    {
        if(CantaJugador1)
        {
            CantaJugador1 = false;
            CantaText.text = "2";
        }
        else
        {
            CantaJugador1 = true;
            CantaText.text = "1";
        }

        EstadoActualEnvido = EstadoEnvido.EnvidoCantado;

        EstadoBoton (BotonEnvido, false);
        EstadoBoton (BotonQuiero, true);
        EstadoBoton (BotonNoQuiero, true);
        EstadoBoton (BotonEnvidoEnvido, true);
        EstadoBoton (BotonRealEnvido, true);
        EstadoBoton (BotonFaltaEnvido, true);

        ActualizarEstadoBotonFlor ();
    }

    public void TocoBotonRealEnvido()
    {
        ActualizarQuienCanta();

        switch (EstadoActualEnvido)
        {
            case EstadoEnvido.SinCantar:
                EstadoActualEnvido = EstadoEnvido.RealEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoRealEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoEnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoEnvidoRealEnvidoCantado;
                break;
            default:
                break;
        }

        EstadoBoton(BotonEnvido, false);
        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
        EstadoBoton(BotonEnvidoEnvido, false);
        EstadoBoton(BotonRealEnvido, false);
        EstadoBoton(BotonFaltaEnvido, true);
    }

    public void TocoBotonEnvidoEnvido()
    {
        ActualizarQuienCanta();

        EstadoActualEnvido = EstadoEnvido.EnvidoEnvidoCantado;

        EstadoBoton(BotonEnvido, false);
        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
        EstadoBoton(BotonEnvidoEnvido, false);
        EstadoBoton(BotonRealEnvido, true);
        EstadoBoton(BotonFaltaEnvido, true);
    }

    public void TocoBotonFaltaEnvido()
    {
        ActualizarQuienCanta();

        switch (EstadoActualEnvido)
        {
            case EstadoEnvido.SinCantar:
                EstadoActualEnvido = EstadoEnvido.FaltaEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoFaltaEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoEnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoEnvidoFaltaEnvidoCantado;
                break;
            case EstadoEnvido.RealEnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.RealEnvidoFaltaEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoRealEnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoRealEnvidoFaltaEnvidoCantado;
                break;
            case EstadoEnvido.EnvidoEnvidoRealEnvidoCantado:
                EstadoActualEnvido = EstadoEnvido.EnvidoEnvidoRealEnvidoFaltaEnvidoCantado;
                break;
            default:
                break;
        }

        EstadoBoton(BotonEnvido, false);
        EstadoBoton(BotonQuiero, true);
        EstadoBoton(BotonNoQuiero, true);
        EstadoBoton(BotonEnvidoEnvido, false);
        EstadoBoton(BotonRealEnvido, false);
        EstadoBoton(BotonFaltaEnvido, false);
    }

    public void TocoBotonQuiero()
    {
        if (JuegaJugador1)
        {
            CantaJugador1 = true;
            CantaText.text = "1";
        } else
        {
            CantaJugador1 = false;
            CantaText.text = "2";
        }

        if (EstadoActualEnvido != EstadoEnvido.YaJugado && EstadoActualEnvido != EstadoEnvido.SinCantar)
        {
            ChequearEnvidoQuerido ();
            EstadoBoton(BotonQuiero, false);
            EstadoBoton(BotonNoQuiero, false);
            EstadoBoton(BotonEnvidoEnvido, false);
            EstadoBoton(BotonRealEnvido, false);
            EstadoBoton(BotonFaltaEnvido, false);
        }
        else if (EstadoActualTruco != EstadoTruco.SinCantar)
        {
            EstadoBoton(BotonQuiero, false);
            EstadoBoton(BotonNoQuiero, false);
            EstadoBoton(BotonTruco, false);
            EstadoBoton(BotonReTruco, EstadoActualTruco == EstadoTruco.TrucoCantado);
            EstadoBoton(BotonValeCuatro, EstadoActualTruco == EstadoTruco.RetrucoCantado);
        }
    }
    public void TocoBotonNoQuiero()
    {
        if (JuegaJugador1)
        {
            CantaJugador1 = true;
            CantaText.text = "1";
        } else
        {
            CantaJugador1 = false;
            CantaText.text = "2";
        }

        if (EstadoActualEnvido != EstadoEnvido.YaJugado && EstadoActualEnvido != EstadoEnvido.SinCantar)
        {
            ChequearEnvidoNoQuerido();

            EstadoBoton(BotonQuiero, false);
            EstadoBoton(BotonNoQuiero, false);
            EstadoBoton(BotonEnvido, false);
            EstadoBoton(BotonEnvidoEnvido, false);
            EstadoBoton(BotonRealEnvido, false);
            EstadoBoton(BotonFaltaEnvido, false);
        } else if (EstadoActualTruco != EstadoTruco.SinCantar)
        {
            ChequearTrucoNoQuerido();

            EstadoBoton(BotonQuiero, false);
            EstadoBoton(BotonNoQuiero, false);
            EstadoBoton(BotonTruco, false);
            EstadoBoton(BotonReTruco, EstadoActualTruco == EstadoTruco.TrucoCantado);
            EstadoBoton(BotonValeCuatro, EstadoActualTruco == EstadoTruco.RetrucoCantado);
        }
    }
    public void TocoBotonFlor()
    {
        if (JuegaJugador1)
        { 
            Jugador1.MisPuntos += VALOR_FLOR;
        } else
        {
            Jugador2.MisPuntos += VALOR_FLOR;
        }

        EstadoBoton (BotonFlor, false);
        EstadoBoton (BotonEnvido, false);
        EstadoBoton (BotonEnvidoEnvido, false);
        EstadoBoton (BotonRealEnvido, false);
        EstadoBoton (BotonFaltaEnvido, false);
    }
  
    public void DarTurno()
    {
        JuegaJugador1 = Random.value >= 0.5f;
        CantaJugador1 = JuegaJugador1;

        if (JuegaJugador1)
        {
            TurnoText.text = "1";
            CantaText.text = "1";
        } else
        {
            TurnoText.text = "2";
            CantaText.text = "2";
        }
    }

    public void ActualizarTextoPuntajes()
    {
        TextoPuntaje1.GetComponent<Text>().text = "JUG1: " + Jugador1.MisPuntos.ToString();
        TextoPuntaje2.GetComponent<Text>().text = "JUG2: " + Jugador2.MisPuntos.ToString();
    }
 
    public void ChequearTrucoNoQuerido()
    {
        int Puntos = 0;

        switch (EstadoActualTruco)
        {
            case EstadoTruco.SinCantar:
                break;
            case EstadoTruco.TrucoCantado:
                Puntos = VALOR_TRUCO_NO_QUERIDO;
                break;
            case EstadoTruco.RetrucoCantado:
                Puntos = VALOR_RETRUCO_NO_QUERIDO;
                break;
            case EstadoTruco.ValeCuatroCantado:
                Puntos = VALOR_VALECUATRO_NO_QUERIDO;
                break;
            default:
                break;
        }

        if (CantaJugador1)
        {
            Jugador1.MisPuntos += Puntos;
            Ganaste1.SetActive (true);
        } else
        {
            Jugador2.MisPuntos += Puntos;
            Ganaste2.SetActive (true);
        }
    }

    public void ChequearTruco()
    {
        if (EstadoActualTruco != EstadoTruco.SinCantar)
        {
            int Puntos = 0;
            switch (EstadoActualTruco)
            {
                case EstadoTruco.TrucoCantado:
                    Puntos = VALOR_TRUCO_QUERIDO;
                    break;
                case EstadoTruco.RetrucoCantado:
                    Puntos = VALOR_RETRUCO_QUERIDO;
                    break;
                case EstadoTruco.ValeCuatroCantado:
                    Puntos = VALOR_VALECUATRO_QUERIDO;
                    break;
                case EstadoTruco.SinCantar:
                default:
                    break;
            }

            if (Jugador1.RondasGanadas == 2)
            {
                Jugador1.MisPuntos += Puntos;
                Ganaste1.SetActive (true);
            } else if (Jugador2.RondasGanadas == 2)
            {
                Jugador2.MisPuntos += Puntos;
                Ganaste2.SetActive (true);
            }
        }
    } 

    public void ChequearEnvidoNoQuerido()
    {
        Jugador JugadorGanador;

        if (CantaJugador1)
        {
            JugadorGanador = Jugador1;
        } 
        else
        {
            JugadorGanador = Jugador2;
        }

        switch (EstadoActualEnvido)
        {
            case EstadoEnvido.EnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.RealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_REAL_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoRealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_REALENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoRealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_REALENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.FaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_FALTA_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_FALTA_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.RealEnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_FALTA_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoRealEnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoRealEnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_REAL_ENVIDO_FALTA_ENVIDO_NO_QUERIDO;
                break;
            default:
                break;
        }

        EstadoActualEnvido = EstadoEnvido.YaJugado;
    }

    public void ChequearEnvidoQuerido()
    {
        int ValorJugador1 = Jugador1.ObtenerValorEnvido ();
        int ValorJugador2 = Jugador2.ObtenerValorEnvido ();
        Jugador JugadorGanador;

        if (ValorJugador1 > ValorJugador2)
        {
            JugadorGanador = Jugador1;
        } else if (ValorJugador2 > ValorJugador1)
        {
            JugadorGanador = Jugador2;
        } else
        {
            // Empate = Gana la mano
            JugadorGanador = Jugador1;
        }

        switch (EstadoActualEnvido)
        {
            case EstadoEnvido.EnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_QUERIDO;
                break;
            case EstadoEnvido.RealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_REAL_ENVIDO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoRealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_REALENVIDO_QUERIDO;
                break;
            case EstadoEnvido.EnvidoEnvidoRealEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_ENVIDO_ENVIDO_REALENVIDO_QUERIDO;
                break;
            case EstadoEnvido.FaltaEnvidoCantado:
            case EstadoEnvido.EnvidoEnvidoFaltaEnvidoCantado:
            case EstadoEnvido.EnvidoEnvidoRealEnvidoFaltaEnvidoCantado:
            case EstadoEnvido.EnvidoFaltaEnvidoCantado:
            case EstadoEnvido.EnvidoRealEnvidoFaltaEnvidoCantado:
            case EstadoEnvido.RealEnvidoFaltaEnvidoCantado:
                JugadorGanador.MisPuntos += VALOR_FALTA_ENVIDO_QUERIDO;
                Imagen.SetActive (true);
                Texto.SetActive (true);
                if (JugadorGanador == Jugador1)
                {
                    MostrarMensajeGanoRondaJ1 ();
                } else
                {
                    MostrarMensajeGanoRondaJ2 ();
                }
                break;
            default:
                break;
        }

        EstadoActualEnvido = EstadoEnvido.YaJugado;
    }

    public void ChequearRondas()
    {
        TurnoJugador1Completado = false;
        TurnoJugador2Completado = false;

        if (EstadoActualTruco == EstadoTruco.SinCantar)
        {
            if (Jugador1.RondasGanadas == 2)
            {
                Jugador1.MisPuntos += VALOR_MANO;
                Ganaste1.SetActive(true);
            }
            else if (Jugador2.RondasGanadas == 2)
            {
                Jugador2.MisPuntos += VALOR_MANO;
                Ganaste2.SetActive(true);
            }
        }
        else
        {
            ChequearTruco();
        }
    }
}
