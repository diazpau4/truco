using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Jugador : MonoBehaviour
{

    public List<Carta> MisTresCartas;
    public ArrayList CartasRestantes;
    public Mazo oMaz;
    public GameObject PrefabCarta;
    public int NumeroCarta;
    public TrucoManager tm;
    public int MisPuntos;
    public int RondasGanadas;
    

    public int Turno;

    public void Inicializar(List<Carta> MisTresCartas)
    {
        NumeroCarta = 0;
        this.MisTresCartas = MisTresCartas;
        foreach (Carta item in MisTresCartas)
        {
           
            GameObject VisualCarta = Instantiate(PrefabCarta);
            var MiValor = VisualCarta.transform.Find("Valor");
            var MiPalo = VisualCarta.transform.Find("Palo");
            var MiNumero = VisualCarta.transform.Find("Numero");
            MiValor.GetComponent<Text>().text = item.Valor.ToString();
            MiPalo.GetComponent<Text>().text = item.Palo.ToString();
            MiNumero.GetComponent<Text>().text = item.Numero.ToString();
            VisualCarta.transform.parent = GameObject.Find("Canvas").transform;
            switch (NumeroCarta)
            {
                case 0:
                    VisualCarta.transform.localPosition = new Vector3(-306.1f, -54.4f, 0);
                    break;
                case 1:
                    VisualCarta.transform.localPosition = new Vector3(-92.9f, -54.4f, 0);
                    break;
                case 2:
                    VisualCarta.transform.localPosition = new Vector3(111.5f, -54.4f, 0);
                    break;
                default:
                    break;
            }
            
            VisualCarta.name = item.Numero.ToString() + item.Palo.ToString();
            item.VisualGameObj = VisualCarta;
            NumeroCarta++;
        }
        
   
    }

    public void ActivarCartas(bool Activar)
    {
        foreach (Carta item in MisTresCartas)
        {
            item.VisualGameObj.SetActive(Activar);
        }
    }
    
    public int ObtenerValorEnvido()
    {
        int Valor = -1; 
        if (MisTresCartas[0].Palo == MisTresCartas[1].Palo)
        {
            Valor = MisTresCartas[0].Numero + MisTresCartas[0].ValorEnvido + MisTresCartas[1].Numero + MisTresCartas[1].ValorEnvido;
        }
        else if (MisTresCartas[0].Palo == MisTresCartas[2].Palo)
        {
            Valor = MisTresCartas[0].Numero + MisTresCartas[0].ValorEnvido + MisTresCartas[2].Numero + MisTresCartas[2].ValorEnvido;
        }
        else if (MisTresCartas[1].Palo == MisTresCartas[2].Palo)
        {
            Valor = MisTresCartas[1].Numero + MisTresCartas[1].ValorEnvido + MisTresCartas[2].Numero + MisTresCartas[2].ValorEnvido;
        }
        return Valor;
    }

    public bool TieneFlor ()
    {
        return MisTresCartas[0].Palo == MisTresCartas[1].Palo && MisTresCartas[1].Palo == MisTresCartas[2].Palo;
    }

    void Update()
    {
        
    }
}
