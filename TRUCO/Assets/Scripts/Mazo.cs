using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Mazo : MonoBehaviour
{
    private List<Carta> lCarta = new List<Carta>();
    private List<Carta> lCartaTemp = new List<Carta>();
    public TextMeshPro txt;
    public Canvas canv;   

    public void AgregarCartas()
    {
        lCarta.Add(new Carta(1, "Espada", 1, 10));
        lCarta.Add(new Carta(2, "Basto", 1, 10));
        lCarta.Add(new Carta(3, "Espada", 7, 10));
        lCarta.Add(new Carta(4, "Oro", 7, 10));
        lCarta.Add(new Carta(5, "Oro", 3, 10));
        lCarta.Add(new Carta(5, "Copas", 3, 10));
        lCarta.Add(new Carta(5, "Espada", 3, 10));
        lCarta.Add(new Carta(5, "Basto", 3, 10));
        lCarta.Add(new Carta(6, "Oro", 2, 10));
        lCarta.Add(new Carta(6, "Copas", 2, 10));
        lCarta.Add(new Carta(6, "Espada", 2, 10));
        lCarta.Add(new Carta(6, "Basto", 2, 10));
        lCarta.Add(new Carta(7, "Oro", 1, 10));
        lCarta.Add(new Carta(7, "Copas", 1, 10));
        lCarta.Add(new Carta(8, "Oro", 12, 0));
        lCarta.Add(new Carta(8, "Copas", 12, 0));
        lCarta.Add(new Carta(8, "Espada", 12, 0));
        lCarta.Add(new Carta(8, "Basto", 12, 0));
        lCarta.Add(new Carta(9, "Oro", 11, 0));
        lCarta.Add(new Carta(9, "Copas", 11, 0));
        lCarta.Add(new Carta(9, "Espada", 11, 0));
        lCarta.Add(new Carta(9, "Basto", 11, 0));
        lCarta.Add(new Carta(10, "Oro", 10, 0));
        lCarta.Add(new Carta(10, "Copas", 10, 0));
        lCarta.Add(new Carta(10, "Espada", 10, 0));
        lCarta.Add(new Carta(10, "Basto", 10, 0));
        lCarta.Add(new Carta(11, "Copas", 7, 10));
        lCarta.Add(new Carta(11, "Basto", 7, 10));
        lCarta.Add(new Carta(12, "Oro", 6, 10));
        lCarta.Add(new Carta(12, "Copas", 6, 10));
        lCarta.Add(new Carta(12, "Espada", 6, 10));
        lCarta.Add(new Carta(12, "Basto", 6, 10));
        lCarta.Add(new Carta(13, "Oro", 5, 10));
        lCarta.Add(new Carta(13, "Copas", 5, 10));
        lCarta.Add(new Carta(13, "Espada", 5, 10));
        lCarta.Add(new Carta(13, "Basto", 5, 10));
        lCarta.Add(new Carta(14, "Oro", 4, 10));
        lCarta.Add(new Carta(14, "Copas", 4, 10));
        lCarta.Add(new Carta(14, "Espada", 4, 10));
        lCarta.Add(new Carta(14, "Basto", 4, 10));
    }

   
    public void NuevoJuego()
    {
        lCartaTemp = lCarta;
    }

    public List<Carta> DameTresCartas()
    {
        List<Carta> CartasReturn = new List<Carta>();
        for (int i = 0; i <= 2; i++)
        {
            int CartaRandom = Random.Range (0, lCartaTemp.Count);
            CartasReturn.Add(lCartaTemp[CartaRandom]);
            lCartaTemp.RemoveAt(CartaRandom);      
        }
        return CartasReturn;
    }
    private void Awake()
    {
        // 5525 : jugador 1 y 2 tienen flor
        // 2468 : jugador 1 tiene flor
        //  : jugador 2 tiene flor
        // Random.InitState (Random.Range(0,9999));
        // Debug.Log (Random.seed);

        lCartaTemp = lCarta;
        AgregarCartas();
    }
}


