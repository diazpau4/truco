using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carta : MonoBehaviour
{
    public int Valor;
    public string Palo;
    public int Numero;
    public int ValorEnvido;

    public enum Paloo { Oro, Espada, Basto, Copas};
    public Paloo PaloEnum;

    public GameObject VisualGameObj;
    public bool FueUsada;
    public Carta(int V, string P, int N, int En)
    {
        Valor = V;
        Palo = P;
        Numero = N;
        ValorEnvido = En;
    }
}
